# IC 2013-14 Sample code #

Welcome to the Interpretation and compilation sample code
repository. This is the first step in the code made available. It
serves as working material for the Interpretation and Compilation
course at FCT/UNL. 

You will find several branches, that represent different (incomplete)
stages of development of the interpreter being developed in the course
lab classes. At each stage, you will find code written in different
programming languages, worked to better suit your taste, capabilities,
and skills.
